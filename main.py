from selenium import webdriver
import time as time

browser = webdriver.Chrome("chromedriver.exe")
browser.get("https://sensorweb.com.br/")
browser.maximize_window()
time.sleep(5)

links = {'soluções': '//*[@id="menu-item-91"]/a', 'clientes': '//*[@id="menu-item-90"]/a',
          'parceiros': '//*[@id="menu-item-93"]/a', 'vagas': '//*[@id="menu-item-85"]/a',
          'contato': '//*[@id="menu-item-550"]/a'}

dados_pessoais = {'Lucas Bergamin Dos santos': '//*[@id="rd-text_field-kqhgpZENZlmb7ThC2Dbo0Q"]',
                  'lbergaminsantos2020@gmail.com': '//*[@id="rd-email_field-QbPb52n6msZrGi_CIfdPKw"]',
                  '19982359150': '//*[@id="rd-phone_field-lbE-L3BllD-dYsU5dSmv9A"]',
                  'SensorWeb': '//*[@id="rd-text_field-3m5nZYUM1HV5X6qbaRPFrg"]',
                  'Futuro desenvolvedor Python JR': '//*[@id="rd-text_field-Oc9lZxCXW1XViYTmUIhu8Q"]'}

for page, link in links.items():
    print(f'A página acessada é {page}')
    browser.find_element("xpath", link).click()
    time.sleep(5)
    if page == 'contato':
        for dado, pagina in dados_pessoais.items():
            browser.find_element("xpath", pagina).send_keys(dado)
    else:
        browser.back()